use LWP::Simple;

sub cmd_gif {

        my ($param, $server, $witem) = @_; #script call parameters
        # param - command line parameters
        # server - the active server in window
        # witem - the active window item (eg. channel, query)



        if(!$witem || !$server) {
                Irssi::print("No active channel window");
                return;
        }
               

        my $data = get("http://webpoint.fi/gif/images/");



        if(my @matches = $data =~ m/(".*$param.*[gif|jpg|jpeg|pdf]")/ig) {

                #regexp operation =~returns first matching pattern in $1
                # then remove quotation marks from filename




                my $shortest;

                foreach(@matches) {
                      
                        if ( !(defined $shortest)) {                 
                                $shortest = $_;
                        }
                        else {
                                if (length($_) < length($shortest) ) {
                                        $shortest = $_;
                                }
                        }

                }

                $shortest = substr $shortest, 1, -1;

                # then simply print out the result
                $witem->command("MSG ".$witem->{name}." http://webpoint.fi/gif/images/$shortest");
        }
        else {
                Irssi::print("Sorry, no gif matches $param");
        }
}

Irssi::command_bind('gif', 'cmd_gif');